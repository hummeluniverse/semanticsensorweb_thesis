%% ch_evaluation.tex
%%

%% ==================
\chapter{Evaluation}
\label{ch:evaluation}
%% ==================

In every single step of this thesis there difficult questions on how
to bring sensor data into the semantic web have been addressed already.
The following sections rate the whole situation of semantic sensor
data and the proposed development process under different angles.


\section{Describing sensors in RDF/XML}

There are many approaches of describing sensor types, some as mighty
and complex ontologies. Thus most of the
common sensors and there data can be described. These descriptions
are useful to categorize sensors and to find the appropriate sensors
automatically or manually. This facilitates the growth of sensor networks
and the crawling of various sensor data.

Nonetheless complex influences of sensors on each other or relationships
among them are complicated to handle. Automatic reasoning might be
hard or impossible when complicated ontologies have to be used.

A similar issue comes with some characteristics like accuracy and
(allowed) ranges of sensors. These two attributes are often important
for scientific research, but they are hard to implement. Customized
modeling and processing might be possible in either way whereas automated
and independent handling of the data becomes impossible. Moreover,
when probabilities come into consideration the use of current tools
and models is a challenging endeavor. (see for example \cite{Calder2010})


\section{Describing measurements in RDF/XML}

Handling the different measurements of sensors strongly depends on
the existing ontologies in the corresponding field, even if there
are some sensor ontologies that cover measurements and observations
as well [Section \ref{sec:existing_approaches_and_ontologies} (\nameref{sec:existing_approaches_and_ontologies})]. Are they well-modeled
and easy to understand or to extend, then they will facilitate extending
the data amount in the semantic web.

If the focus is on the measurements, the idea of the Eventweb provides
a good solution of handling time and space in easy models. It might
even allow many forms of reasoning and inferring on the data. Nonetheless
complex structures stay complicated and the future development of
n-ary predicates might be necessary - not only in the sensor context.

Unit handling leads to another problem, if not addressed in the used
ontologies. If modeled with custom properties, reasoning and processing
would be kept easy but the combination of different sources or the
automatic conversion of units is not practical at the moment.


\section{Live data handling}

There seem to exist many projects that try to facilitate streaming
of Linked Data. It might not take a long time until there will be
solutions that work pretty well, as there are existing solutions in
traditional web services already. The difficulty is the interchangeability
of the different approaches, one of the former targets of SOA\footnote{SOA - Service Oriented Architecture}.
Some standardized interfaces or service descriptions are needed
to allow spiders or query processors to use different methods without
the need of customized code for each solution. This might as well
be an important w3c standard.


\section{Developer benefits}

As described in this work, the modular structure of semantic web applications
allows developers to concentrate on parts of the software. If they
use some different core applications, programming languages or even
generic software, they should be very fast and flexible in common
use cases. The trending agile development philosophy fits perfectly
well the idea of the semantic web.

The modeling of ontologies is, however, very time consuming and may
be complicated. Some problems mentioned in the first two sections
play important roles and there are many others that can be found in
literature. Even so, the Linked Open Data community and much of the
Web 2.0 enthusiasm as well as the Open Source movement refrains from
the 'not invented here' attitude and reduces the overall effort
of modeling ontologies. Using existing ontologies should be capable
as well for developers who are not deeply involved in ontology engineering.


\section{Stakeholder benefits}

Coming from the developers to the more abstract stakeholders such
as data providers, managers or analysts with a special focus on the
people or companies that own or maintain the physical sensors.

A lot of sensors and sensing devices
that could be connected are property of people or companies that might
not be willing to share their data for several reasons, especially
privacy or money.

In case anything else than a scientific or governmental organization
should take part in a sensor network they most often don't want to
put much effort or even money in publishing their data, especially
if they do not generate any return on invest. This may be the reason
for some sensor data publishers to use proprietary formats like a
custom csv. The effort is very low and by chance they might be able
to sell some specifications on that by chance whereas they do not
benefit from putting more effort in publishing Linked Data. The only
stakeholders that tend to benefit directly from well-prepared data
seem to be the analysts that can generate results with low effort
and can present their results on sites with advertisements or can
even sell them.

Furthermore private data should only be shared in between selected
parties, for example business partners, friends or medical institutions.
Like with the OAuth used in the pubsubhubbub, there are some existing
concepts, but unless they are becoming standard, safe and easy to
use, many potential sensor data providers will most probably not participate.

Another important part for managing quality and trust is the use of
cryptography or digital signatures. Especially if automatic crawling
replaces manual sorting of sources someday, it should be at least
traceable who created and altered information, that automatic rating
of relevance becomes possible. This would additionally give at least
some credits to the stakeholders. See for example \cite{Thuraisingham2010, Alam2008} or tRDF\footnote{tRDF \url{http://trdf.sourceforge.net/}}.


\section{Example evaluation/Lessons learned}
\margexample
When creating the different modules the architecture proposed in chapter \ref{ch:generalapproach} (\nameref{ch:generalapproach}) was very helpful due to the abstract structure for different systems.

However, Google App Engine restricted work in some ways, because it did not support all frameworks that would have been useful. A Jetty or Tomcat deployment could have been better. Nonetheless, developing demos on App Engine is comfortable, amongst other things because of the good Eclipse integration and their development framework.

When referring to the performance of the demo system some lessons learned can be presented. The complete time measurements can be found in the \nameref{ch:appendix} \ref{sec:time_measurements_of_example_application} (\nameref{sec:time_measurements_of_example_application}).
The most important result is that the generated RDF should be saved  or cached on the system somehow. The creation of the web-output takes more than half of the total conversion time. Triggering the conversion with a cronjob and caching the result for real requests should be okay. Saving the results in a file or in a triple store could avoid caching problems and should be fast enough as well.
Besides of that it may also be useful to cache source files either for the csv2rdf conversion or in the further process (here the kml-creation).

Concerning real-time data the best option might be streaming. In the example generating 2.100 triples out of a local csv-source took about 1 second. Real sensors might produce a multiple of that amount and if one aggregates several resources it gets even more important.

Regarding the KML-creation the time killer is the creation of the Jena model out of the existing RDF-data since it takes about half of the total creation time. Maybe another framework like the NxParser\footnote{NxParser \url{http://code.google.com/p/nxparser/}} would be better as it supposedly ``ate 2 mil. quads (~4GB, (~240MB GZIPped)) on a T60p (Win7, 2.16 GHz) in ~1 min 35 s (1:18min).''

Above all, the integration with dbpedia's SPARQL-endpoint worked quite well and can be recommended. However, in the data for the sample query might not change that often and therefore the data should be stored locally to avoid unnecessary requests.