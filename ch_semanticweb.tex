%% ch_semanticweb.tex
%%

%% ##############
\chapter{Sensor Data in the Semantic Web and Linked (Open) Data}
\label{ch:semanticweb}
%% ##############

First of all semantic technologies are just another kind of handling
data and their success depends on how widely they are used. Nonetheless,
there are some difficulties with traditional file or data formats
and content handling that are addressed with semantics. This chapter
will also give a short introduction on the technologies and will then
focus on existing sensor ontologies.


\section{Target}

Without describing semantic technologies in detail this section treats
some basics, since they are relevant for the whole work and addresses
the question whether we actually need semantics or not. According
to \cite{Pileggi2010} ``the main limitation for a concrete realization
of Sensor Web appears the lack of standardization that make the interoperability
among systems a hard challenge also considering the functional interoperability
environment provided by last generation web services''.

Traditionally the structure of a file (binary or csv) or its syntax
(xml) is used to access the contents and extract or transform the
data. The description of the data can be found in specification documents
that have to be written and maintained for every data format and the
tools have to be customized for exactly that formats.

With the use of some standardized xml-documents data exchange in selected
fields is becoming easier. Nonetheless, it even may be very complicated
to merge two standards.

The semantic technologies try to add a meaning to every resource,
regardless on the position in a hierarchy or structure of a file.
One often talks about Linked Data, because the main idea is to describe
data with the help of other data that is described already.

This avoids redundancy and leads to more efficient ways of working.
On the one hand, a data provider can concentrate on the core data
and on the other hand a data consumer does not need to handle several
different data formats.

Additionally the semantics can be processed by machines, in comparison
to specifications in pdf format, and the use of crawlers provides
the opportunity of collecting more informative or additional data.

Even if machines are not able to understand the data they can detect
correlations and infer hidden knowledge. With the help of some custom
algorithms they sometimes even seem to understand the data and can
perform useful actions.

Besides wonders in artificial intelligence, one can imagine many ways
of data selection, data preparation or data presentation, ranging
from dynamically faceted searches to the analysis of complex, previously
unknown, connections in the data.


\section{Semantic Technologies}

In this thesis the selected semantic web technologies defined by the
w3c are RDF and OWL.It is assumed that the reader has some basic knowledge
of them, otherwise more detailed information can be found in \cite{Hitzler2009}
or \cite{Hebeler2009}, for example.

RDF and OWL share many common characteristics so that this thesis
tries to handle them as one technology whenever possible. Concerning
a special use case one might need the expressivity of OWL Full or
the clarity of RDF combined with the different inference possibilities.

Some difficulties in relation to the usage with sensor data come up
when trying to model events in time and space, two of the important
values describing a measurement. The traditional handling of the data
works well when describing single facts. One example might be something
like 
\begin{verbatim}
ex:AngelaMerkel rdf:type ex:Bundeskanzler
\end{verbatim}

Here the problem is that Angela Merkel is only Bundeskanzlerin (federal chancellor) of a certain country (Germany)
and for a certain time interval (since 2005 until now). Bundeskanzler could fit as well to the head of states of Austria or Switzerland or historic German federations and is not intended to describe only the current persons in charge but also the former people in charge. This however can not easily be modeled in a simple triple.
There are several possible solutions,
some described in chapter \ref{ch:modelingdata} (\nameref{ch:modelingdata}). If one would
just collect data for a certain field one could work with that, for
example with some more detailed properties like \verb+ex:presidentOfTheUS+,
but if it comes to data in motion like you have with sensor data,
every new statement is only usable with the related observation time
and sometimes location. Hence a solution has to be found.

Unit handling is another difficult aspect of current standards. At
the moment there is official support for xml-schema datatypes but
it lacks physical units. Some solutions will be presented as well in
chapter \ref{ch:accessingdata} (\nameref{ch:accessingdata}).

One additional fact that every developer or modeler should bear in
mind is the open world assumption and the non-unique name assumption,
maybe in combination with blank nodes. This is important for modeling
and even more necessary to consider when debugging unexpected behavior
when reasoning.

Especially blank nodes can be problematic in inference whereas one
should keep in mind that URIs describing a resource are only allowed
to describe a single resource whereas two different URIs do not necessarily
refer to distinct resources.

Thus using resolvable URIs as recommended there might be the necessity
to distinguish the URIs somehow, for example by the use of OWL2 keys.


\section{Existing Approaches and Ontologies}
\label{sec:existing_approaches_and_ontologies}

Since the years around 2005 experts have tried to develop approaches
to semantify the sensor data. In the state of the art two main approaches
are used to describe sensor data: Annotating existing formats or converting
to raw RDF/OWL triples.

Most often the annotation is used when working with already standardized
conventional formats, especially the Sensor Web Enablement (SWE) xml-files.
Languages used for that are for example XLink or RDFa that can link
to semantic web ontologies and therefore enable the Linked Data functionality.
A short example of annotation is provided in the following code-example
of \cite{Sheth2008}.

\lstinputlisting[caption=Example for Annotating SWE-XML with RDFa]{code/annotated_swe.xml}

\subsection{Sensor Ontologies}
Of course one needs a sensor ontology as well and the expressivity
depends on it mainly. Nonetheless there are some difficulties when
describing more complex data in comparison to a total conversion that
does not require a predefined structure and can also use other source
formats.

Concerning the existing ontologies there are many different targets
as well. The main difference seems to be the description of sensors
and networks in contrast to observations and measurements.

Pileggi is proposing \textit{A Noval Domain Ontology for Sensor Networks}
\cite{Pileggi2010} that provides the ability of describing detailed
sensor characteristics like the location of a sensor host, the sensor
in relation to the host and the possible change of these positions
over time. Additionally one could define communication modes or energy
supply as well as the nature of the sensors itself (multisensors).
This could be used when searching for sensors for a certain purpose.

A outstanding approach in describing the structure of sensor networks
is done with the SWAMO Ontology that is intended to create intelligent
agents ``for collaboration between multiple sensor systems''\cite{Underbrink2011}.
Observations and Measurements as defined in the SOS are implemented
very minimalistic ``for minimal compliance with the standard''.

Eleven sensor ontologies from 2009 or older have been evaluated by
Compton et al. \cite{Compton2009a} in range and expressivity
as well as reasoning and search technology. According to them ``the
state of the art is some way from the the vision for semantic networks''.
This is reasoned especially because of the lack of ontologies that
cover most aspects of sensor data including measurement data. The
most advanced ontologies in that category seem to be CSIRO \cite{Compton2009, Neuhaus2009a} and OntoSensor
with a slightly different focus. A combination of them is said to
represent the current level of expressive capability for semantic
sensors.\\
Most evaluated ontologies used a sensors perspective and are missing
observation models. Besides OntoSensor the ontologies Avancha \cite{Avancha2004} seems to provide a useful base of observation
handling including data/observation features, accuracy and support
for units. They also propose the results of Florian Probst who concentrated
on semantifying the observation and measurement standards of the OGC.
\cite{Probst2006, Probst2008}

Many of the authors
of the existing ontologies formed an Incubator group at the w3c to
develop a new ontology, the SSN-XG\cite{Compton2012} ontology that is used nowadays by
many new projects and might be the future de-facto standard ontology
for sensor networks, because of their spread and closeness to the SWE standards. A sample definition of an accelerator device is shown in the following code listing:

\lstinputlisting[caption=Example of a SSN-XG device description (RDF/OWL)]{code/ssn-xg_defining_device_with_measurement_unit.rdf}

According to \cite{Taylor2011} the SSN-XG ontology design offers four identfiable perspectives:

\begin{itemize}
	\item ``Sensors, with a focus on what senses, how it senses, and what is sensed;''
	\item ``Data, with a focus on observations and metadata;''
	\item ``Systems, with a focus on systems of sensors; and''
	\item ``Features, with a focus on physical features, properties of them, what can sense them, and what observations of them are made.''

\end{itemize}

\subsection{APIs}
Finally there is still the question of how to access the data in the
ontologies.

SemSOS \cite{Henson2009} is following the existing standards and tries
to combine the existing service API with a semantic backend. This
means that traditional queries like ``DescribeSensor'', ``GetObservation''
and ``GetCapabilities'' are transformed into SPARQL queries to retrieve
the information out of the knowledgebase. The response is using the
standard xml-schema for observations and measurements and is semantically
annotated with XLink, using a self-developed O\&M-OWL-Ontology.

In 2011 Page et al. \cite{Page2011} proposed a prototype of a Web
API relying on REST and Linked Data principles in combination with
the SSN-Ontology. The \textit{High-Level API for Observations (HLAPI)}-platform
allows to provide several representations and serializations of the
data, including non-semantic formats, and supports several additional
URIs like /latest or /summary for special requests.

\subsection{Summary}
Summarized the way of annotating existing structures like SWE might
lead to fast results. Using traditional semantic web methods, however,
might keep the interoperability between different fields better what
is very important in that environment.

Some important aspects on the modeling will be discussed in chapter
\ref{ch:modelingdata} (\nameref{ch:modelingdata}) whereas the discussion about accessing
is dealt with in chapter \ref{ch:accessingdata} (\nameref{ch:accessingdata}). In particular
there will be the question on how to implement live transfer of data.