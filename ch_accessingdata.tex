%% ch_accessingdata.tex
%%



%% ##############
\chapter{Accessing the Data}
\label{ch:accessingdata}
%% ##############

Accessing the previously converted data is one of the main targets
a semantic sensor data application should be focused on.

Three common access methods can be determined: Polling, Notifying
and Streaming.

\begin{figure}[h]
\begin{centering}
\includegraphics[width=0.8\textwidth,bb = 0 0 580 130, clip]{grafiken/ppt/accessing_details_boxed.png}
\par\end{centering}

\caption{Schematic view of the module \textit{Accessing} \label{fig:structure-accessing-details}}
\end{figure}

\section{APIs}

First the developer has to decide which fundamental principles
to use for data access.

As described in chapter \ref{ch:sensordata} (\nameref{ch:sensordata}) existing sensor networks are based
on service architectures like the SOS of the SWE. This architecture
provides specific functions for a certain environment. Especially
communication between sensor networks could be improved using technologies
like SOAP. Developments like SemSOS extend existing service structures
and provide backwards compatibility as well as the option to use Linked
Data.

Alternatively there is the REST architecture with prominent use in
combination with the semantic web. The focus on well-described stateless
methods and the resource orientation fits well with the Linked Data
Principles\footnote{Linked Data Principles \url{http://www.w3.org/DesignIssues/LinkedData.html}}.
RESTful architectures are lightweight and easy reusable. The resources
can be identified and linked with their corresponding URIs and are
therefore a good choice to manage information.

\section{Static Data Access}

\subsection{Polling}
Polling is the standard action used to obtain resources in the web.
Talking in REST terms, a client sends a GET message to the server.
Using specific headers one can request special response formats and
the server can deliver meta information about the response (e.g. expiration
times or sizes). Standard implementations are redirects or other status
codes for communication over http with a standardized vocabulary.
In particular, the client can request a certain resource multiple
times without side-effects like state changes.\cite{Tilkov2009}. An architecture based on
polling lacks informing the client on changes even if there are possibilities
save resources on polling requests when nothing has changed. The implementation
is straight forward because of the presence in every framework or
tool. Developing a RESTful application needs some additional effort
but is not complicated.

Since the proceeding of live data is one of the scenarios sensors
are in, it is important to provide support for that in the web architecture.
Polling in fixed intervals is useful if those intervals are known
before and the data that has to be fetched is not too much. Handling
customized requests asking for modified data only are difficult but
could be achieved with a service structure that filters data according
its data, for example. A possible solution would be an Atom/RSS-Feed
that informs about new data and saves traffic due to its size.

\section{Live Data Access}
Polling however does not allow the server to open up a connection. According to a blog post\footnote{XMPP PubSub or Pubsubhubbub for real-time server push? \url{http://www.leggetter.co.uk/2010/09/17/xmpp-pubsub-or-pubsubhubbub-for-real-time-server-push.html}} of Phil Leggetter, a developer focused on real-time applications, there are three ways of publishing live-data in the web:

\begin{itemize}
	\item "If you want your data in real-time you should use a persistent connection between the publisher and subscriber."
	\item "If you are making a server to server subscription to data that does not update all that often and instant real-time doesn't matter then PubSubHubbub is fine."
	\item "If you are making a server to server subscription to data that updates very frequently then you need to use a persistent connection and XMPP PubSub is a must."
\end{itemize}

\subsection{Notifying}
\label{subsec:notifying}
In that sense a good extension to polling is to enable communication between server
and client, especially when the update interval is asynchronous. That
way traffic is reduced and the load on the server is minimized.The
idea is to use a notification service that sends out messages from
the server to the registered clients and triggers a polling mechanism.
thus existing technology with all its benefits can be kept and only
has to be extended by a service on the client side. However, that
form of a listener must be a server that is active the whole time
and servers behind firewalls can lead to problems. Furthermore the
communication messages are not standardized, even if the protocol
is. Additionally new data could also be sent over the new push-communication.
Furthermore a notification architecture is very scalable when new hubs are added.
\subsubsection{XMPP}
One solution would be the instant messaging protocol XMPP. Besides other features the XMPP Standards Foundation proposes their Publish-Subscribe draft\footnote{XEP-0060: Publish-Subscribe \url{http://xmpp.org/extensions/xep-0060.html}} because of the classic observer pattern that allows the server to send one message to multiple subscribers. Since ``XMPP is a fairly complex standard, which is often too heavy for the limited resources of embedded devices used in sensor networks.''\cite{Guinard2011} there is a similar pubsub protocol that tends to get into the focus of developers: Pubsubhubbub.

\subsubsection{Pubsubhubbub}

The Pubsubhubbub protocol\footnote{Pubsubhubbub \url{https://code.google.com/p/pubsubhubbub/}}
allows publishers to notify a hub about new content and directs
the spread including subscription handling to the hub. Moreover different categories are supported and clients can register for specific ones.
In the context of the semantic sensor web architecture is used for example in the Linked Sensor Middleware\cite{Le-Phuoc2011}.
Pubsubhubbub uses the idea of WebHooks. That means HTTP posting data to an HTTP endpoint's callback URL.\footnote{What WebHooks are and why you should care  \url{http://timothyfitz.wordpress.com/2009/02/09/what-webhooks-are-and-why-you-should-care/}} \footnote{What are WebHooks and How Do They Enable a Real-time Web? \url{http://blog.programmableweb.com/2012/01/30/webhooks-realtime-web/}}

\subsection{Streaming}
Streaming data is the closest option to real-time data handling. Existing Linked Data technologies have to be extended to be able to process live data. Besides using non-standards technologies like the WebHooks there are some ideas on how to stream linked data. See for example the blog post by Greg Brail in retrospect to the ReadWriteWeb real-time summit.\footnote{Greg Brail - Hooks, Sockets and Firehoses: Streaming API Technologies Getting Us to REALLY Real-Time \url{http://blog.apigee.com/taglist/Webhooks}}

There are many ways to stream data. Most probably one will produce RDF/XML and is therefore able to use XML streaming solutions like the Streaming API for XML (StAX)\footnote{Streaming API for XML \url{http://stax.codehaus.org/Home}}.

Furthermore there are the new HTML5 WebSockets\footnote{websocket.org \url{http://www.websocket.org}}\footnote{w3c websocket draft \url{http://dev.w3.org/html5/websockets/}} or, as Google provides on App Engine, the Channel API \footnote{GAE Channel API \url{https://developers.google.com/appengine/docs/java/channel/overview}} with Comet-style that might be replaced with WebSockets later on. Sometimes even an HTTP-connection is left open and the server continues to send new data.

However, the most complicated part is to parse streaming data. Some examples of Data Stream Management Systems (DSMS) are provided in section \ref{sec:handling_semantic_sources} (\nameref{sec:handling_semantic_sources}) in the following chapter. They most often can create streams on their own to create stream networks.

\section{Storage}

Depending on the application's objectives and the chosen access method
one or more of the three options might fit for the storage solution.
For the sake of completeness some short discussion is provided here.

\begin{figure}[h]
\begin{centering}
\includegraphics[width=0.8\textwidth,bb = 0 0 480 150, clip]{grafiken/ppt/storing_details_boxed.png}
\par\end{centering}

\caption{Schematic view of the module \textit{Storing} \label{fig:structure-storing-details}}
\end{figure}

\subsection{Direct Conversion}
First of all no storage solution is required at all. This might be
preferred when only live data is required and the amount of queries
is limited. With the help of caching techniques the load could be
managed efficiently. Restrictions are primarily that due to the need
for caching only uncustomized access is possible if there are many
requests. Additional post-processing possible so that specific data
access can be implemented in a later stage. This would also be a good
solution for streaming when only a limited number of clients receives
the data and provide their access methods.

\subsection{File}
Secondly saving the converted data as file enables the clients to
save the URL for later reference. Overview pages enable access to
historic files and could facilitate access to parts of the data, depending
on the file structures. Notifying solutions fit perfectly but customized
queries are inefficient because the whole file has to be parsed first.
Besides of that, a storage mirror or an archive could be set up easily.

\subsection{Database/Tripe Store}
A database or triple store would be the third option and can provide
many benefits. With the matching structure it may be as fast as files
and supports customized queries on top while frequent ones could be
cached as well. If loads of customized data is generated it even might
reduce traffic when only relevant triples are queried and sent instead
of whole files. Compared to the previous options the effort for choosing
the perfect data server can be very high, not only because of additional
functions like SPARQL support but also because of distinct optimizations.
On the one hand may be useful to use a write-optimized datastore because
of the huge amounts of data generated by the sensors and the live
aspect and on the other hand a good query performance seems to be
necessary. Sometimes integrated inference methods can speed up the
following process when detecting critical measurements or related
data. Automatically added statements can simultaneously lead to problems
when removing data from the store if they are nested too deep. One
could use 4 store\footnote{4store \url{http://4store.org/}}, cumulusrdf\footnote{cumulusrdf \url{http://code.google.com/p/cumulusrdf/}}
or some large triple store that can be found at the w3c wiki\footnote{Large Tripe Stores \url{http://www.w3.org/wiki/LargeTripleStores}}.


\section{Example}
\margexample

The first decision fell on a REST-Style API that should fit best with
existing Linked Data technologies. In Java one could use some existing
frameworks like Restlet\footnote{Restlet \url{http://www.restlet.org/}}
or the reference implementation for JAX-RS\footnote{JAX-RS \url{http://download.oracle.com/otndocs/jcp/jaxrs-1.0-fr-oth-JSpec/}}
Jersey\footnote{Jersey \url{http://jersey.java.net/}}. In combination
with Google App Engine there are some little challenges even if Restlet
should work quite well for the basic features, so that some basic
implementation like content-negotiation has been implemented with
the use of mimeparse\footnote{mimeparse \url{http://code.google.com/p/mimeparse/}}
to handle the accept headers. Resolvable property-URIs allow interlinking
with other ontologies. Resolvable measurements are not fully implemented,
because live data is not saved at the moment.

Since the update interval is known to be five minute there is no need
to stream data and notifying services would be preferred. ``Additionally
Google App Engine does not support sending data to the client, performing
more calculations in the application, then sending more data. In other
words, App Engine does not support 'streaming' data in response to
a single request''\footnote{GAE Java Servlet Environment \url{https://developers.google.com/appengine/docs/java/runtime}}.
Google proposes and supports the use of the XMPP protocol\footnote{GAE Using the XMPP Service \url{https://developers.google.com/appengine/articles/using_xmpp}} whereas the Pubsubhubbub-protocol might have its benefits as well.
A high-performance and real-time live streaming, however, would be possible with jWebSocket\footnote{jWebSocket \url{http://jwebsocket.org/}} that does not work on App Engine because of the socket restrictions but you can use Tomcat, for example.\footnote{jWebSocket in Webapps \url{http://jwebsocket.org/howto/ht_webapp.htm}}\footnote{jWebSocket Server on Application Servers \url{http://jwebsocket.org/quickguide/qg_appserver.htm}}