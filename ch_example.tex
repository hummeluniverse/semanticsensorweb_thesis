%% ch_example.tex
%%

%% ==============
\chapter{Example -- Aviation Weather Data}\margexample
\label{ch:example}
%% ==============
The focus of this chapter lies on the demo web application that contains
some example implementations of the topics covered in this thesis.
In detail the decision for the sensor data of NOAA's aviationweather.gov
will be provided combined with a description of the data sources.

Concrete implementations will be presented in the following chapters
that discuss the theoretical solutions first and an applied solution
afterwards.


\section{Data set}


\subsection{Requirements}

For most developers the following requirements will be obsolete, because
they will use a predefined data set. However, for demonstration purposes
some characteristics of the data are important:
\begin{description}
\item [{Use~publicly~available~data}] Everybody should have the opportunity
to follow the steps throughout the whole application. Especially the
processed data should be accessible, for example reuse of code for
training purposes is facilitated.
\item [{Use~simple~data}] Easy understandable data reduces unnecessary
complicated code structures in the example code and the reader can
focus on the important points. Moreover, if the data is self-explanatory,
one does not need much effort to understand the examples.
\item [{Use~data~that~is~expandable}] Since this is a semantic web
topic, most data will be linked and enriched with other data sources.
Demo data should provide the ability to create easy usage scenarios
and mashups.
\end{description}
In case of the sensor data one could determine some additional statements:
\begin{description}
\item [{Use~sensor~data~with~at~least~one~measurement~unit}] The
purpose of sensors is to provide measurements. Even in a sample application
the handling of the different formats and units should be mentioned.
\item [{Use~frequently~updated~data}] An additional characteristic of
sensor-measurements is that they happen several times and the observation
time matters most often. Therefore a timestamp would be useful and,
furthermore, the performance topics can be treated. This also leads
to thoughts about the necessity of live or even real-time usage of
the data.
\item [{Use~~geo-located\_sensors~or~data}] The location of the measurement
is important in many cases. Geo-location will suit for many scenarios
in an open system. Moreover the usage of moving sensors adds another
interesting fact to be concerned.
\end{description}

\subsection{NOAA and the Aviation Weather Center}

The National Oceanic and Atmospheric Administration (NOAA)\footnote{NOAA:\url{http://www.noaa.gov/}} of the
United States Department of Commerce does research in many fields
of oceans, atmosphere and the climate.

One department called the National Weather Service (NWS)\footnote{NWS: \url{http://weather.gov/}} operates
the Aviation Weather Center\footnote{Aviation Weather Center: \url{http://aviationweather.gov/}}. This center provides forecasts and warnings for the global aviation and publishes several measurements that focus
on temperature, wind speeds, icing, visibility and many more. Furthermore
the data is used by pilots for their pre-flight briefings.\cite{AIM2012}

Since the weather is probably one of the most topics spoken of, it
is predestined for demo data. Furthermore one can think of a huge
amount of usage scenarios. Therefore two datasets have been chosen:
the METAR data and the aircraft reports. Each of the data sets will
be described more detailed in the following sections.

Both data sets have in common that they contain time stamps as well
as geo-coordinates for each measurement. Even if the observation intervals
can be irregular the latest data is updated every five minutes and
can be accessed as csv- or xml-file. The dataserver \footnote{Dataserver: \url{http://www.aviationweather.gov/adds/dataserver}} contains descriptions
about all offered data sets and queries for historic data as well
as current data files\footnote{Current data files: \url{http://www.aviationweather.gov/adds/dataserver/current}}. 

For further reference, the Aironautical Information Manual covers huge parts of the data pilots use and create \cite{AIM2012}.

\subsection{Meteorological Airfield Report}

The Meteorological Airfield Report (METAR) is a standardized format
for local reports or local special reports (SPECI) of weather observations
and forecasts. Standards and regulations necessary for global aviation
are set by the International Civil Aviation Organization (ICAO)\footnote{ICAO: \url{http://www.icao.int/}},
an agency of the United Nations (UN).

A METAR report represents an hourly observation of a specific site
regarding several climatic conditions manually or automated. Most often the location is
an airport, represented by its ICAO-Code. the measurements are published
as custom string that contains further information about location,
observation time, temperature, wind speed and many more. However,
in this example a preprocessed csv-file is used - the detailed field
description can be found in the \nameref{ch:appendix}
(Table \ref{tbl:metarfielddescription}). (See also \cite{FMH1-2005} and \cite{AIM2012})

Important aspects for utilization of this data source in the example
are:
\begin{itemize}
\item ICAO airport codes are widely used, so further information about airports
like departure times or encyclopedic facts could be aggregated.
\item The sensors are not moving but are fixed to a certain location. This
allows the easy monitoring of a specific place over time. Furthermore
it might facilitate geographic searches in the data.
\item Many applications can be found that allow analysis of METAR data and
can serve as model for useful semantic mashups. See NOAA's Java Tool
or Meterradar.com
\end{itemize}
The current data set, with observations of the past hour, contains
relative constantly more than 4000 measurements of different airports
with 44 possible single data values. Hence around 350 new measurements
are added in every update interval.


\subsection{Aircraft reports}

The second data source for the demo application are the aircraft reports that contain either Pilot Weather Reports (PIREPs) or Aircraft Reports (AIREPs).

The data of these reports must only be published if there are special
conditions like thunderstorms, turbulences, bad visibility or dramatic
changes in weather conditions.

The reported string fields differ slightly from the METAR fields and
more details on the preprocessed format structure can be found in
the \nameref{ch:appendix} (Table \ref{tbl:pirepfielddescription}). (See also \cite{FMH12-1998} and \cite{AIM2012})

Important aspects for utilization of this data source are:
\begin{itemize}
\item Aircrafts are moving objects (with an additional altitude) that make
great demands on the processing application. Maybe even the tracking
of a specific aircraft would be possible.
\item The data is as easy to interpret as the METAR data. However, a real-life
task would be the automatic generation of warnings for aviation.
\item Many observations are sent in the region between Canada and Greenland
over the North Atlantic Ocean. Here the use of spatial queries could
be interesting.
\end{itemize}
The current data set, with observations of the past ninety minutes,
contains highly fluctuating numbers of measurements but during the
last analyses always significantly below 1000, because they are requested
or transmitted most often only in special situations. Thus one can
state that there will be less than 100 new measurements on average
every update interval. In contrast to the METAR data the appearance
of the same sensor/aircraft multiple times has a higher probability.

\section{Other Data Sources}
Another marine example is provided with the data of \url{http://www.marinetraffic.com}: "`The system is based on AIS (Automatic Identification System). As from December 2004, the International Maritime Organization (IMO) requires all vessels over 299GT to carry an AIS transponder on board, which transmits their position, speed and course, among some other static information, such as vessel’s name, dimensions and voyage details."'\
One is allowed to query every two minutes while the received data is updated in their system in real time.