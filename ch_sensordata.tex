%% ch_sensordata.tex
%%

%% ##############
\chapter{Sensors, Sensor Data and the World Wide Web}
\label{ch:sensordata}
%% ##############


\section{Definition}

There are several definitions for sensors; most often they describe
the observation of ``a physical quality (temperature, depth, etc)
of a feature (a lake) and report observations''\cite{Compton2009a}
but in a looser way one could also refer to ``a data source which
produces a sequence of data items over time''\cite{Le-Phuoc2009}.
In \cite{Henson2009} a sensor is only seen as a certain procedure
to produce data.

Those loose definitions, however, do not focus on the 'classical'
view of a sensor as a physical device that measures a single physical
quantity. In fact, there may be several use cases where the measured
data is already aggregated in a virtual sensor like a platform or
digital mashup with several physical sensors that act as a single
sensor. Furthermore non-physical measurements like CPU load, GPS or
even camera pictures can be addressed with the latter definition.

This allows the utilization of many sensors with similar features
and characteristics. Nonetheless, the main focus in this work will
be on the observations of physical devices that measure physical quantities.
It is is easier to concentrate on a restricted set of items, even
if most of the characteristics equal the nature of the extended second
sensor definition.


\section{Main Characteristics of a Sensor}

Overall there are two main elements a sensor consists of: First there
is the sensing device or platform and its specification and secondly
there are the measurements and observations of this sensor basis,
both often referred to as sensor data.

See for example the specifications for a temperature sensor by AADI
in figure \ref{fig:temperature_sensor_specs} for measuring the water temperature.
If someone is searching for a sensor that can be used in a specific
environment the operating temperature, operating depth or the dimensions
may be important. For someone who deals with the measured data it
may be more useful to use information about the resolution or accuracy
of the generated data.

\begin{figure}[h]
\begin{centering}
\includegraphics[scale = 0.9, bb = 0 0 250 380, clip]{grafiken/temperature_sensor_basic_specifications.jpg}
\par\end{centering}

\caption{AADI Temperature Sensor 4880/4880R}\label{fig:temperature_sensor_specs}


\end{figure}


In general the information relevant for future utilization of the
generated observation data is the location of the sensor. Even if
the location in restricted environments can be very detailed and complex,
this thesis will concentrate on the geo-referenced information only.
However, it is be possible that the location is changing over time,
for example when the sensor is attached to a car. Then the positions
can be seen as an additional measurement and must be observed.

Very important for analyzing the data beyond that are the instants
of time when the observation was made, since time is used most often
as a dimension for evaluating or visualizing data.

Furthermore, most measured values, especially physical, are useless
without the corresponding units (unless one is in a separated environment).

These three main attributes should be kept in mind when working with
sensor measurements and observations. In some cases the influence
of accuracy is very important, too; in particular when there are scientific
evaluations. Here considering a possible change of the accuracy under
certain circumstances is important.

If one assumes the correct implementation and use of the sensor, most
of the other technical details tend to be redundant. It may be useful
to keep operating ranges or response times in mind.


\section{Short Classification of Sensors}

The classification of sensors in a holistic way is cumbersome and
will not lead to a huge improvement in handling sensor data. Nonetheless,
there are some points that might be good to know.

First of all there is the accessibility of the data: Are specifications
and measurements publicly available or is there restricted access
because of privacy, security or commercial purpose?

Directly connected to that question is the second point: Which role
plays the sensor owner? Is it a scientific organization, a company
or even a private person? This gets even more complicated if one goes
into usage rights an licenses.This often depends from the field of
application like described in a classification scheme derived from
a Hitachi Research Laboratory communication\cite{White1987}. Automotive,
Energy, Health or Transportation are mentioned, for example.

Weather measurements are an example for globally publicly available
data whereas positions of mobile phones may be difficult to collect.

In this thesis there are two additional characteristics that play
a significant role, as mentioned in the previous section: Does the
sensor move or is it geographically fixed? And how important are fast
update rates or is it a real-time sensor?


\section{Use Cases for Sensors on the Web}

Publishing sensor data in digital formats for further processing with
Information and Communication Technology is a common use case. Therefore
an easy way is using the world wide web that allows remote control
of sensors as well as an automated aggregation of measurement data.
Automated actions on behalf of specific events are far from vision.

Some people tend to speak of the \textit{Internet of Things} (see \ref{sec:about_the_topic} \nameref{sec:about_the_topic}) that tracks
products with RFID codes and sensor data. If we had some automatism, things could be identified by machines and collect data about them with sensors, technology would be able to notify us about necessary repair or help us to use resources more effective and efficient.

And the global trend is following these ideas: On this years CeBIT
trade show, one of the largest computer expos in the world, one main
topic has been the smart home. The five subtopics are Home Automation,
Home Networks, Home Entertainment, Smart Grid/Energy Management and
Home Appliances/Design\footnote{CeBIT Smart Home \url{http://www.cebit.de/en/about-the-trade-show/programme/cebit-life/smart-home}}.
Especially the influences of the Smart Grid will be interesting in
the future, because machines will be able to use energy when its cheapest
or when regenerative energy can be used. Another use case of the ambient
assistant living solutions would be the automatic opening of the windows
for air refreshment and closing them when it begins to rain. Moreover,
a third example is the opening of the garage when one comes home by
car and after that the front door unlocks automatically as well.

Automated systems in cars are another recent topic in the news in
Europe. From 2015 onwards every new car should contain an automated
emergency call system, named eCall. In the case of an accident, the
system will automatically send necessary information via internet
to the ambulance\footnote{heise.de: eCall \href{http://www.heise.de/newsticker/meldung/eCall-Auto-Notruf-soll-ab-2015-fuer-Pkw-verbindlich-werden-1631991.html}{http://www.heise.de/newsticker/meldung/eCall-Auto-Notruf-soll-ab-2015-fuer-Pkw-verbindlich-werden-1631991.html}}.
And this is only a start: The car 2 car communication consortium\footnote{car 2 car communication consortium \url{http://www.car-to-car.org/}}
is trying to achieve standards in inter-vehicle communication systems
for safety, ecological and efficiency reasons.

Regardless of these interesting designs and developments much data
is generated already. Navigation systems collect data of velocities
or routes and share them amongst each other, to avoid traffic jams
or inform the driver about security risks. Indeed, traffic jams are
sometimes detected due to mobile phone devices and their movements
in certain regions\footnote{Bild der Wissenschaft - Mit Handys gegen den Stau \url{http://www.bild-der-wissenschaft.de/bdw/bdwlive/heftarchiv/index2.php?object_id=31994303}}.

The last example topic will be the private use of sensor data. People
are able to track their sports activities with their smartphones\footnote{Android App Sports Tracker \url{http://www.androidpit.de/de/android/market/apps/app/com.sportstracklive.android.ui.activity.lite/SportsTracker-by-STL}},
share their weight/muscle amount on fitness networks\footnote{Health Graph API \url{http://blog.runkeeper.com/new-feature/health-graph}}
or let their apps automatically post music, pictures, activities or
locations to the world wide web\footnote{Google+ Party Mode \url{http://support.google.com/plus/bin/answer.py?hl=en&answer=2618786}}\footnote{Pearson: Using Facebook and Spotify together\url{http://www.quepublishing.com/articles/article.aspx?p=1833572&seqNum=2}}.

There are many more samples, but these show several aspects of sensor
categories, data that may never be published for free or data that
might become very important in the future. What semantifying of the
data can achieve, will be discussed in the later.


\section{Existing Sensor Standards}
\label{sec:existing_sensor_standards}

Concerning the combination of sensors and their integration into the
web, there is mainly one standard used: The Sensor Web Enablement (SWE)\footnote{OGC - Sensor Web Enablement \url{http://www.opengeospatial.org/ogc/markets-technologies/swe}}
by created by the Open Geospatial Consortium (OGC), a member of the
w3c.

They define five main fields developed relying on standards of
the IEEE or other organizations to enhance the creation of reusable
technologies.

\begin{minipage}[t]{1\columnwidth}%
\begin{description}
\item [{Observations~\&~Measurements~(O\&M)}] - ``The general models and
XML encodings for observations and measurements.''
\item [{Sensor~Model~Language~(SensorML)}] - ``standard models and XML
Schema for describing the processes within sensor and observation
processing systems.''
\item [{PUCK}] - ``Defines a protocol to retrieve a SensorML description,
sensor 'driver' code, and other information
from the device itself, thus enabling automatic sensor installation,
configuration and operation.''
\item [{Sensor~Observation~Service~(SOS)}] - ``Open interface for a web
service to obtain observations and sensor and platform descriptions
from one or more sensors.''
\item [{Sensor~Planning~Service~(SPS)}] - ``An open interface for a web
service by which a client can 1) determine the feasibility of collecting
data from one or more sensors or models and 2) submit collection requests.``
\end{description}
\begin{flushright}
{\scriptsize Descriptions taken from http://www.opengeospatial.org/ogc/markets-technologies/swe}
\par\end{flushright}%
\end{minipage}

Concerning the topic of existing live data systems, the inproceeding \textit{Providing near Real-time Traffic Information within Spatial Data Infrastructures} \cite{Mayer2009} seems to be interesting. They state that ``Service Oriented Architectures (SOA) constitute the main paradigm for developing GI\footnote{Editors Note: GI = Geographic Information} applications nowadays''. In this case the SOS is used.