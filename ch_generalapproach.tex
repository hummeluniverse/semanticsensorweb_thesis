%% ch_generalapproac.tex
%%

%% ==============
\chapter{General approach and structure of code}
\label{ch:generalapproach}
%% ==============

In this chapter some fundamental procedures on how to develop a semantic
web application will be presented. Firstly there will be a short explanation
of the proposed structure of implementations and secondly there will
be listed some possible stakeholders. After that some fundamental
questions for preparing a semantic web project and finally the decisions
in our sample program are mentioned.


\section{Modular structure of code}

Semantic web technologies with their standardized exchange formats
make it easy to work with modular software. On the one hand this enables
software developers to use several distinct programming languages
as well as platforms that fit best for the particular task. For example
one could use Python to process text sources and use Java for the RDF handling, as in the tutorial by Bob DuCharme\cite{DuCharme2010}.

On the other hand the development process can focus on single modules.
Therefore agile software development practices suit very good and
can provide quick results as well as constantly expendable fragments
of the application.

The proposal of a theoretical structure of a sensor data processing
semantic web application is shown in the following schema. 

\begin{figure}
\begin{centering}
\includegraphics[width=0.8\textwidth,bb = 0 0 600 250, clip]{grafiken/ppt/overall_structure_filled.png}
\par\end{centering}

\caption{Proposed modular structure for a semantic sensor data web application\label{fig:Proposed-modular-structure}}
\end{figure}


Thus three main modules can be identified in figure \ref{fig:Proposed-modular-structure}:
data collection, data access and further processing of the data. Data
storage may be seen as module number four but it is not necessary
in all cases. Every module will be discussed in detail in the corresponding
chapter.

Literature researches propose very similar structures: Hebeler et
al. propose an architecture diagram for aggregating disparate data
sources with the layers ``Data sources'', ``RDF Interfaces'',
``Domain Translation'' and ``Knowledgebase'' \cite[p. 468]{Hebeler2009}.
This model is a subset of the newly proposed modules ``Collecting''
and ``Storing'' and may be convenient for some use cases. Additionally
the focus on aggregating different sources in one model and would
perfectly fit in the new modules of the further processing.

An additional architecture is proposed for the Linked Sensor Middleware
by Le Phouc et al. \cite{Le-Phuoc2011}


\section{Stakeholders}

This abstract architecture proposal is very flexible and is good for
a step-by-step or partially implementation of the modules. There are
many different stakeholders that might not be interested in implementing
a holistic application.
\begin{description}
\item [{Case~0:~Sensor~Data~Provider}] A data provider maintains sensors
and owns the data sources. Most often the data is used for internal
use and therefore prepared for that purpose. Existing data formats
tend to not containing semantics or semantic annotations.\\
Sensor Data Provider are typically scientific or governmental organizations,
because they publish their data to the web. (e.g. NOAA\footnote{NOAA \url{http://noaa.gov.us}}
Dealing with all possible sensor owners these might as well be organizations
or even private persons.
\item [{Case~1:~Semantic~(Sensor)~Data~Provider}] Adding semantics
to the sensor data is the task of a semantic data provider. The better
they work together with the sensor owners, the more accurate the results
will be. Most sensor data provider fear the complexity of annotating
or converting their data, because effort increases with complexer
sensor structures and dependencies.\\
Modeling depends not only on the input data, possible use cases have
to be sorted out to find appropriate ontologies and other Linked Data
to map with. Designing models that fit to the data and choosing the
correct ontologies is one of the most time consuming tasks. However,
the interlinked data serves as basis for further processing. The better
the data is presented, the more valuable it will be.\\
Nowadays many conversions are done by the semantic web community and
corresponding research institutes. Nonetheless, in the future sensor
owners might publish their data as RDF/OWL right away.
\item [{Case~3:~Data~Manager}] Working with the data requires storage
and access strategies in order to facilitate to generate knowledge
out of several sources. Data Manager try to collect data from several
sources, create mappings between ontologies and resources and store
the data in customized databases for quick access. Different accessing
and querying possibilities enable programmers to use the best way
to get the data, without the need of searching for data that matches
the existing one.\\
Dbpedia\footnote{Dbpedia \url{http://dbpedia.org}} could be seen
as data manager, since it collects data from the various Wikipedias
and links different topics. Most often famous content provider serve
as starting points for crawls since many ontologies try to reuse the
terms and models. A data manager could also aggregate thematic data
to reduce the effort for individuals to crawl the web on specific
topics.
\item [{Case~4:~Data~Analyst}] Many developers use existing data and,
except for quality and trust, they do not care about the sources and
the steps done before. They rely on the division of labor and their
task is to create results from the data. Thus they would like to choose
their querying method and process, enrich or evaluate the received
data. This group of the stakeholders generates new knowledge and is
therefore very important. They sometimes use complex data mining and
knowledge retrieval methods and should generate additional value.
Therefore the data basis must be good enough.
\end{description}
These examples of stakeholders are stereotypes and make discussions
about the importance of different modules easier - they are most often
not distinct actors. Even though, this example shows the common ground
each stakeholder works with: Ontologies and data exchange formats
as well as the APIs to access data.

This means that the modeling of the data is one of the most important
steps in the whole process. Bad models will lead to bad results. Reusing
popular ontologies will facilitate the reuse of the data by others.

Of quite similar importance are the exchange formats. Since there
are many standardized formats like RDF/XML or query-languages like
SPARQL, a stakeholder should not have problems handling it.

Nonetheless, the data access can be more complex. Using RESTful web
applications is an easy step whereas the use of services, especially
to handle live data, can be an intricate structure. Especially for
sensor data scenarios the live aspect is very important and the presence
of new data must be communicated fast enough. At the moment there
exist no de-facto standards in the semantic web environment. Some
possible solutions are discussed in the chapter \ref{ch:accessingdata} (\nameref{ch:accessingdata}),
but they often do require a customized interface.


\section{Defining targets}

In the following chapters several implementation-approaches of each
modul will be discussed. Depending on the goals there are different
ways that seem useful.

However, there are some very important questions one should bear in
mind before implementing an application in addition to the stereotypes
defined in the section before. Despite of the general discussion for
and against semantic technologies, one could mention the following
categories of questions:
\begin{itemize}
\item Scope~of~the~data

\begin{itemize}
\item How many measurements will be generated?
\item How often are there new measurements?
\item Is it useful to enrich data with important information at the beginning?
\end{itemize}
\item Usage of the data

\begin{itemize}
\item Will the data be used in a whole most probably\\
or will there be many customized queries on parts of the data?
\item Will a client access the data once or is continuous access important?
\item Is it important to keep a history of the data or is there a high significance
of the latest measurements only?
\end{itemize}
\item Time, effort and expenses

\begin{itemize}
\item How detailed and accurate should the data be mapped? (Units, Accuracies,
Dependencies, ...)
\item How much manual interaction should be needed for reasoning and inferring?
\item How much load does the content providing architecture stand?\end{itemize}
\end{itemize}

\section{Example}
\margexample
A short overview about the general tools used for developing will
be shown here. Specific libraries or tools will be mentioned in the
corresponding chapter. Credits go to all the developers and supporters
of the various utilities, even those not mentioned here.
\begin{description}
\item [{Java}] is used in throughout the whole application. A mixture of
programming languages in reality is possible and might be useful to
use the different strengths. \newline \url{https://www.java.com/}
\item [{Eclipse}] is the main IDE for developing the application. \newline \url{http://www.eclipse.org/}
\item [{Notepad++}] suits perfectly for smaller tasks. \newline \url{http://notepad-plus-plus.org/}
\item [{GIT/TortoiseGit}] is used for version control. Hosted on bitbucket. \newline \url{http://git-scm.com/} \newline \url{https://code.google.com/p/tortoisegit/} \newline \url{https://bitbucket.org/}
\item [{Dropbox}] helps for files not under version control. \newline \url{https://www.dropbox.com/}
\item [{Google~App Engine}] with the local Jetty server serves as gate
to the web. Eclipse integration is available beside loads of documentation. \newline \url{https://developers.google.com/appengine/}
\item [{\TeX{}nicCenter~and~\L\kern-.1667em\lower.25em\hbox{Y}\kern-.125emX\@}] are used for the thesis. \newline \url{http://www.texniccenter.org/} \newline \url{http://www.lyx.org/}
\item [{JabRef}] collects all the useful references to literature. \newline \url{http://jabref.sourceforge.net/}
\end{description}
Not to forget all the boards, tutorials and examples in the web. One
important destination probably is \url{stackoverflow.com}.