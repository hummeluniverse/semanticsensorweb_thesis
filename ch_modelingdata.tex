%% ch_modelingdata.tex
%%

%% ##############
\chapter{Collecting and Modeling Sensor Data}
\label{ch:modelingdata}
%% ##############

Whether or not there is a physical sensor it is assured that there
is already some sensor data existent in a proprietary format. This
chapter deals with the handling of the data source as well as the
modeling of the data for the following conversion.

\begin{figure}[h]
\begin{centering}
\includegraphics[width=0.8\textwidth,bb = 0 0 650 200, clip]{grafiken/ppt/collecting_details_boxed.png}
\par\end{centering}

\caption{Schematic view of the module \textit{Collecting and Modeling} \label{fig:structure-modeling-details}}
\end{figure}

\section{Collecting}
In the first step of a semantic sensor web application the data has to be collected. There are basically three different types of sources that one could have to handle: Files, databases and (web) services, either with semantics or without and either static or live.

It is assumed for this section that there is not RDF/OWL source data. The question about the semantics gets relevant in the \nameref{ch:queryingandvisualising} chapter, even though it might already be relevant at this point in reality.

\subsection{File}
A file source is most often updated in a certain interval and has to be polled each time. Sometimes a modified-since header or an expiration-header can reduce traffic as well as the use of a notification-service could do. More about that can be found in the following chapter.
Another characteristic of files is that always the whole file has to be transmitted and most often even the new (live) data is appended to the existing file.

\subsection{Database}
Databases provide the possibility to filter for useful data and reduce load with customized queries. Most often a result can be provided in various formats, sometimes there is even the possibility to avoid converting the whole data and use a connector that allows SPARQL queries on the relational database (see section \ref{sec:conversion} \nameref{sec:conversion}). Apart from that the most effort is the database query with SPAQRL, SQL or another query language.

\subsection{Web Service}
The handling of web services depends on their architecture and API. Sometimes it is possible to request a response-format that can be converted easily later on. Very interesting is the possibility of streaming even though this requires a more complex structure. It even might be one of the fundamental sensor data processing requirements.

\subsection{Other Services}
Depending on how the data of the sensors is distributed one might need special protocols or architectures. Especially when there is no sensor middleware and the devices are sending their data on a low-level basis.

In all cases it is good to avoid too much load on the source servers. Sometimes they have to block clients for that. Giving the application a meaningful identifier and providing contact information would be a nice touch.

\section{Modeling}

In the second step the measurements are modeled. Depending on the
sensor ontology that has been chosen for modeling the sensor features
there are many preferred modeling techniques for the measurements
as well. Sometimes sensor ontologies provide support for modeling
observations, but depending on the topic one might want to link as
well to existing ontologies, like weather or even dbpedia.

Some ontologies address important aspects and difficulties of the
modeling in a certain way. Here some solutions are presented in a
detached form that cover the basic problems of the expressivity in
RDF/OWL as determined in chapter \ref{ch:semanticweb} (\nameref{ch:semanticweb}).

\subsection{Adressing the Limitations of RDF/OWL}
In general the problems of time and space and units have have to be
covered, because each new measurement will be done in a new environment. However, handling those issues can be complicated.

\subsubsection{N-ary Properties}
A heavily discussed solution in many other use cases, as well, is
the use of n-ary predicates that can contain more information than
a regular triple. For example one could define with some sort of annotations
or a special syntax:

\begin{verbatim}
ex:sensor ex:hasTemperature "10"^^xsd:Double @unit(ex:celsius)
	@time(2012-01-01) @latlong(10,51)
\end{verbatim} This is not implemented yet but some projects address problems like
that -- for example there were some approaches in the Semantic Desktop
community\footnote{Semantic Desktop Nepomuk \url{http://www.semanticdesktop.org/ontologies/}}. See also \footnote{Defining N-ary Relations on the Semantic Web \url{http://www.w3.org/TR/swbp-n-aryRelations/}}

\subsubsection{Blank Nodes}
A solution that definitely works is the use of blank nodes for a measurement.
A benefit is indeed that blank nodes are seen as distinct resources.
Unfortunately adding information to that node later is difficult and
so is reasoning and querying. If the structure is known, however,
most actions are possible. A code example could look like this:

\lstinputlisting[caption=Example measurement using blank nodes (Turtle)]{code/modeling_example1.ttl}

\subsubsection{EventWeb}
Not significantly different is the idea of the EventWeb. Here the
measurement event itself is in the focus of the design and brings
some benefits. The Eventweb\cite{Jain2008, Sheth2008a} focuses primarily on a
happening at a certain time or time frame and location. The view on
the data is more in the way people's mind works and less object orientated.
Assuming that a single measurement is seen as an event, because of
the ``eventdriven nature of sensor readings''\cite{Page2011},
there is no problem with adding relevant data to that event. Exchanging
the blank node with a unique URI, one could model something like the
following:

\lstinputlisting[caption=Example measurement using EventWeb-desgin (Turtle)]{code/modeling_example2.ttl}

The URI for that event allows automatic reasoning as well as the possibility
to resolve that URI to receive all existing information. Depending
on the generation of the URI (random or semantic), it would be possible
to add data parallel or in distributed architectures, although the
further processing should not rely on the name of the URI, since URIs
should not contain information read by machines. Future additions
are also possible, for example quality control remarks or inferring
results. A human-friendly URI could be something like \verb+http://example.org/id/measurements/2012/01/01_15h34m12s+,
but this should only simplify life for the developers.

Quite often the measurement data is the most used data and therefore
the nature of the sensors are not relevant. This design integrates
good into the existing structures and technologies. Depending on the
scenarios one might want to define that each measurement is different
from the others, referring to the non-existing Unique Name Assumption.

A great amount of observation and measurement ontologies use the Eventweb
style but restrict every event to a single physical measurement. Then
one could avoid properties with the unit encoded in the name and add
something like \texttt{ex:measurementEvent12345 ex:hasUnit ex:Celcius}.

\subsection{Location}
Regardless to where location information is written, however, there
are some additional aspects that are important.

Not all measurements refer to one point. Think of a basin with a temperature
that is identical in the whole basin. One might want to describe this
with the help of a geometric form, e.g. a polygon, then a more detailed
model might be useful. Thinking of future queries it might even be
useful to use a descriptive format even for points (e.g. geo:Point).
If furthermore, by accident, two or more Points are added to a measurement
resource, the allocation of the corresponding latitude and longitude
is impossible.

See also the w3c basic wgs84 geo vocabulary\footnote{W3C Basic Geo Vocabulary \url{http://www.w3.org/2003/01/geo/}},
NEOGEO \footnote{NEOGEO \url{http://geovocab.org/doc/neogeo.html}}
or GeoRSS\footnote{GeoRSS \url{http://www.georss.org/simple}}. Especially
when handling with altitudes there are loads of different standards
that might to lead to problems when using a different standard, like
the wgs84.

\subsection{Time}
The time information focuses very similar problems. Here the time
can be an instant, point or an interval, for example. Two different
time properties can lead to confusion as well if they do not describe
different events like ``observation time'' and ``local observation
time''. Here the w3c provides a good solution with the use of OWL-Time\footnote{OWL-Time \url{www.w3.org/TR/owl-time}}.
A good solution would be to use a custom URI like ``ex:hasObservationTime''
that is described in detail when getting resolved, to avoid bloating
up the data.

\subsection{Units}
Finally the handling of units can be a problem as well, since there
is no standard existing at the moment. Many ontologies try to
define units in their measurement events, as described above. At the
moment this seems to be the best solution, because there is no other
widely used approach. The SSN ontology, for example, "`does not contain its own model for these concepts and does not restrict the user in the choice of an ontology to model them."' They propose\footnote{Report Work on the SSN ontology \url{http://www.w3.org/2005/Incubator/ssn/wiki/Report_Work_on_the_SSN_ontology}} the MyMobileWeb Measurement Units Ontology\footnote{MyMobileWeb Measurement Units Ontology \url{http://purl.oclc.org/NET/muo/muo}}, the QUDV ontology\footnote{QUDV ontology \url{http://www.omgwiki.org/OMGSysML/doku.php?id=sysml-qudv:quantities_units_dimensions_values_qudv}} or the QUDT ontology\footnote{QUDT ontology \url{http://qudt.org/}}.
This usage of "`value containers"' is described in \emph{Semantic Web Programming}\cite[p. 483ff]{Hebeler2009} as "`one of the most flexible, explicit and correct approaches to associating units of measurements with literals values"'. Two other possibilites are the use of unit-specific properties and datatypes or the reification of statements (RDF) respectively annotation properties (OWL).

\subsection{Document Information}
A little note on annotating the data with document information will
close this section. Here one could use the VoiD-Vocabulary\footnote{VoiD \url{http://vocab.deri.ie/void/}}
that is ``intended as a bridge between the publishers and users of
RDF data, with applications ranging from data discovery to cataloging
and archiving of datasets.'' This might be interesting in order to
provide information about authors, publishers or subjects of the dataset.


\subsection{Multidimensional Data and Statistics}
Regarding sensor data, the \textit{RDF Data Cube Vocabulary}\footnote{RDF Data Cube Vocabulary \url{http://www.w3.org/TR/vocab-data-cube/}} could fit even more since it builds on the VoiD-Vocabulary and others. Indeed it is intended to describe multidimensional data like statistics to allow further processing in spreadsheets or OLAP, what is a common use case for sensor data.


\subsection{Ontology Engineering}
When developing a new ontology no one should forget to get some knowledge in ontology engineering before and to prove the validity of the results afterwards. For example aligning the ontology to an upper ontology would be a good factor for reuse.

\section{Conversion}
\label{sec:conversion}

Given the case that the source on hand has no semantics there are three possibilities to enrich the data: Annotation, Conversion in Triple-Format or Storage of the data in a relational database and convert on request.

\subsection{Annotation of existing Data}
Annotating the data works best when they are in a standardized format, because then the annotation process gets easier. Like described in \cite{Compton2009a} or \cite{Henson2009} RDFa or XLink are good choices for annotating XML. They used it in combination with SWE XMLs (SensorML and O\&M - see section \ref{sec:existing_sensor_standards} \nameref{sec:existing_sensor_standards}). Using the Linked Sensor Middleware \cite{Le-Phuoc2011} one can annotate XMLs even in the user interface.

\subsection{Conversion into Triple Format}
Converting data into a triple-format tends to be the common use case. However, the great variety of different files and files structures does not allow to use generic approaches. However, in some restricted use cases this might be possible - see for example the generic CSV-conversion in the demo example (\ref{subsec:example_conversion_method}). Indeed many standard formats can be converted with open-source implementations. See for example the RDF file converter overview\footnote{RDF file converter overview \url{http://www.w3.org/wiki/ConverterToRdf}} or the RDF importers and adapters\footnote{RDF importers and adapters \url{http://www.w3.org/wiki/RDFImportersAndAdapters}}.

\subsection{Database Mapping}
The third option is generating an RDF vocabulary directly out of a database schema or to map it manually is very useful for existing data. According to \cite{Le-Phuoc2011} ``existing triple stores can not efficiently handle high update rates''. Therefore relational databases have been used with the purpose to avoid a time-consuming complete conversion and store the data directly. The provided mapping allows running SPARQL queries on the database similar to the usage in \cite{Page2011}. Likewise other implementations for SPARQL or RDF exist and are listed in the w3c-wiki.\footnote{RDF and SQL \url{http://www.w3.org/wiki/RdfAndSql}} 

\section{Example}
\margexample


\subsection{Data Source Analysis}

Basic descriptions of the data sources can be found in chapter \ref{ch:example} (\nameref{ch:example}):
A five minutes update interval of each csv-file source has been mentioned,
so we could poll the updated files in that intervals.

The new files contain all the data of the past hour (METAR) or one-and-a-half
(aircraft reports) and only the latest ones should be processed. In
the csv-files the measurements are sorted descending by observation
time, so the most recent updates will be at the beginning of the file.
In order to stay flexible the top n lines are read while another possibility
would be to detect which observations are really new. This allows
us to reuse the query classes easily for testing whether there is
new data or not. In production use it might be efficient to select
the new measurements at the beginning or to remove them when they
already exist in the database, for example.

Both example observation files contain timestamps as well as geo-located
points of the measurements taken, so that the most important data
is on hand. The aircraft reports have their altitude as special information,
because there are several practices on how to measure altitudes.

To keep the example simple, only temperature data will be converted
and the rest is ignored, even if it would be not much effort to convert
the other data additionally.


\subsection{Modeling}

Since the demo application is intended to show the whole process and
point out some important points and not to act as a full-featured
data provider, the time consuming process of modeling was reduced
significantly. The only consumer of the data is the corresponding
visualization, so that no additional linking is necessary.

The quick and dirty approach assigns all the data values to a measurement
event defined by latitude, longitude and time. Unit handling is furthermore
intended to be treated with property name (hasTemperatureC).

\lstinputlisting[caption=A sample output of a METAR measurement (Turtle)]{code/noaa_metar.turtle}

Besides the quick and dirty approach the custom properties could be
properly defined and interlinked with other ontologies in their description-files
that can be retrieved by resolving their URL or using external mappings.
Depending on the use case some SWRL rules might have to be used.


\subsection{Conversion Method}
\label{subsec:example_conversion_method}

A direct conversion to RDF/XML can be done very generic, so that the
properties for each field in the csv are defined in a configuration
file. (see \nameref{ch:appendix} -> \nameref{sec:rdfconversionsample})

Furthermore rad RDF/XML is produced besides the formats supported
by the Jena Framework like Turtle or N3. So far no storage solution
is needed and the decision on that can be made according to the access
requirements in the next chapter.

The concrete process, implemented in Java, reads the source file,
crops some information at the beginning and converts the csv to a
String-array Java-list with the help of the openCSV-framework\footnote{openCSV \url{http://opencsv.sourceforge.net/}}.

After that the list will be parsed and each data value with its corresponding
property will be added to a Jena Model. There might be the option
to add some more complex structures, but this has to implemented hard-coded
at the moment. It might be useful for basic definition of time and
space, for example.